import store from 'store';

import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StoreWrapperService {
  set(key: string, value: any) {
    store.set(key, value);
  }

  get(key: string): any {
    return store.get(key);
  }

  remove(key: string) {
    store.remove(key);
  }

  clearAll() {
    store.clearAll();
  }
}
