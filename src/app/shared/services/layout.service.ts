import { StoreWrapperService } from './../../core/services/store-wrapper';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  constructor(private storeWrapperService: StoreWrapperService) {}

  get userName(): string {
    return this.storeWrapperService.get('userName');
  }

  set userName(userName: string) {
    this.storeWrapperService.set('userName', userName);
  }
}
