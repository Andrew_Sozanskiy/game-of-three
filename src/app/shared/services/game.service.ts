import { ITurn, Roles } from 'src/app/shared/models/turn.model';

import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WinnerModalComponent } from '../components/winner-modal/winner-modal.component';

interface ComputerAction {
  total: number;
  action: number;
}
@Injectable({
  providedIn: 'root'
})
export class GameService {
  bsModalRef: BsModalRef;

  private _computer: ITurn[] = [];
  private _player: ITurn[] = [];
  private _currentTurn: Roles;
  private _winner: Roles;

  constructor(private modalService: BsModalService) {
    this.resetGame();
  }

  get computer() {
    return this._computer;
  }

  get player() {
    return this._player;
  }

  get currentTurn(): Roles {
    return this._currentTurn;
  }

  get isPlayerTurn(): boolean {
    return this._currentTurn === Roles.PLAYER;
  }

  get isComputerTurn(): boolean {
    return this._currentTurn === Roles.COMPUTER;
  }

  get isPlayerWinner(): boolean {
    return this._winner === Roles.PLAYER;
  }

  get isComputerWinner(): boolean {
    return this._winner === Roles.COMPUTER;
  }

  get winner(): Roles {
    return this._winner;
  }

  set winner(role: Roles) {
    this._winner = role;
  }

  get lastPlayerTurn(): ITurn {
    if (this.player.length) {
      return this.player[this.player.length - 1];
    }
  }

  get lastComputerTurn(): ITurn {
    if (this.computer.length) {
      return this.computer[this.computer.length - 1];
    }
  }

  playerTurn(turn: ITurn) {
    this._currentTurn = Roles.COMPUTER;
    this._player.push(turn);
    this._player = [...this._player];
    setTimeout(() => this.doComputerTurn(), 3000);
  }

  divide(total: number): number {
    return total / 3;
  }

  resetGame() {
    this._winner = null;
    this._computer = [];
    this._player = [];
    this._computer.push({ total: this.randomizeNumbers(20, 100) });
    this._player.push({ total: this.randomizeNumbers(20, 100) });
    this._currentTurn = Roles.PLAYER;
  }

  openWinnerModal() {
    const initialState = {
      isComputerWinner: this.isComputerWinner
    };
    this.bsModalRef = this.modalService.show(WinnerModalComponent, {
      initialState,
      ignoreBackdropClick: true
    });

    this.bsModalRef.content.onClose$.subscribe((result: boolean) => {
      this.resetGame();
    });
  }

  private doComputerTurn() {
    if (this.isComputerTurn && !Boolean(this.winner)) {
      const computerTurnResult = this.computerAction(
        this.lastComputerTurn.total
      );
      this.computerTurn({
        action: computerTurnResult.action,
        total: computerTurnResult.total
      });
    }
  }

  private computerTurn(turn: ITurn) {
    this._currentTurn = Roles.PLAYER;
    this._computer.push(turn);
    this._computer = [...this._computer];
  }

  private computerAction(total: number): ComputerAction {
    let action: number;
    this.isComputerWin(total);
    switch (total % 3) {
      case 2:
        total += 1;
        action = +1;
        this.isComputerWin(total);
        break;
      case 1:
        total -= 1;
        action = -1;
        this.isComputerWin(total);
        break;
      case 0:
        action = 0;
        total = this.divide(total);
        this.isComputerWin(total);
        break;
    }
    return { total, action };
  }

  private isComputerWin(total: number) {
    if (total / 3 === 1) {
      this.winner = Roles.COMPUTER;
      this.openWinnerModal();
      return;
    }
  }

  private randomizeNumbers(min: number, max: number) {
    return Math.floor(Math.random() * (max - min) + min);
  }
}
