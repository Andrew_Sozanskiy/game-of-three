export interface ITurn {
  total: number;
  action?: number;
}

export enum Roles {
  PLAYER = 'PLAYER',
  COMPUTER = 'COMPUTER'
}
