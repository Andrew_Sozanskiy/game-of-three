import { LayoutService } from './../services/layout.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PlayerStore {
  constructor(private layoutService: LayoutService) {}

  get userName(): string {
    return this.layoutService.userName;
  }
}
