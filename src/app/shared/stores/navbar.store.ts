import { Injectable } from '@angular/core';
import { LayoutService } from '../services/layout.service';

@Injectable({ providedIn: 'root' })
export class NavbarStore {
  constructor(private layoutService: LayoutService) {}

  get userName(): string {
    return this.layoutService.userName;
  }
}
