import { Injectable } from '@angular/core';
import { StoreWrapperService } from 'src/app/core/services/store-wrapper';

@Injectable({ providedIn: 'root' })
export class GameStore {
  constructor(private storeWrapperService: StoreWrapperService) {}
}
