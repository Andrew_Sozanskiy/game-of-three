import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';

import { Component } from '@angular/core';

import { PlayerStore } from '../../stores/player.store';

@Component({
  selector: 'got-winner-modal',
  templateUrl: './winner-modal.component.html'
})
export class WinnerModalComponent {
  isComputerWinner: boolean;

  onClose$: Subject<boolean> = new Subject();

  constructor(public bsModalRef: BsModalRef, public playerStore: PlayerStore) {}

  close() {
    this.onClose$.next();
    this.bsModalRef.hide();
  }
}
