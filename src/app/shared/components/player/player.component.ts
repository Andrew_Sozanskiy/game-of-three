import { Roles } from 'src/app/shared/models/turn.model';

import { Component } from '@angular/core';

import { GameService } from '../../services/game.service';
import { PlayerStore } from '../../stores/player.store';

enum TurnActions {
  PLUS = 'PLUS',
  MINUS = 'MINUS',
  ZERO = 'ZERO'
}

@Component({
  selector: 'got-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent {
  turnActions = TurnActions;

  constructor(
    public playerStore: PlayerStore,
    public gameService: GameService
  ) {}

  turn(action: TurnActions) {
    switch (action) {
      case this.turnActions.PLUS:
        const incrementedTotal = this.gameService.lastPlayerTurn.total + 1;
        this.action(incrementedTotal, 1);
        break;
      case this.turnActions.MINUS:
        const decrementedTotal = this.gameService.lastPlayerTurn.total - 1;
        this.action(decrementedTotal, -1);
        break;
      case this.turnActions.ZERO:
        this.action(this.gameService.lastPlayerTurn.total, 0);
        break;
    }
  }

  private action(newTotal: number, shift: number) {
    if (newTotal / 3 === 1) {
      this.gameService.openWinnerModal();
      this.gameService.winner = Roles.PLAYER;
    } else if (newTotal % 3 !== 0) {
      this.gameService.playerTurn({
        action: shift,
        total: this.gameService.lastPlayerTurn.total + shift
      });
    } else {
      this.gameService.playerTurn({
        action: shift,
        total: this.gameService.divide(
          this.gameService.lastPlayerTurn.total + shift
        )
      });
    }
  }
}
