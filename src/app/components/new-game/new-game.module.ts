import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NewGameComponent } from './new-game.component';
import { SharedModule } from 'src/app/shared/shared.module';

const MODULES = [ReactiveFormsModule, FormsModule, CommonModule, SharedModule];

const COMPONENTS = [NewGameComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
  providers: []
})
export class NewGameModule {}
