import { Component } from '@angular/core';

import { GameService } from '../../shared/services/game.service';

@Component({
  selector: 'got-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent {
  constructor(public gameService: GameService) {}
}
