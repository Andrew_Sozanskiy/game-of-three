import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { LayoutService } from '../../shared/services/layout.service';
import { Router } from '@angular/router';

@Component({
  selector: 'got-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  nameForm: FormGroup;

  constructor(private layoutService: LayoutService, private router: Router) {}

  get isFormInvalid(): boolean {
    return this.nameForm.invalid;
  }

  ngOnInit() {
    this.initForm();
  }

  submit() {
    if (this.nameForm.invalid) {
      return;
    }
    this.layoutService.userName = this.nameForm.get('name').value;
    this.router.navigate(['new-game']);
  }

  private initForm() {
    this.nameForm = new FormGroup({
      name: new FormControl('', [Validators.required])
    });
  }
}
